import requests
import subprocess


access_token = ''
if not access_token:
	raise RuntimeError('Define your access token in the script')
mr_api = f'https://gitlab.com/api/v4/projects/2009scape%2F2009scape/merge_requests?state=opened&scope=all&per_page=100&private_token={access_token}'


def fetch_mr_data() -> list:
    print('getting MR list...')
    response = requests.get(mr_api)
    if response.status_code != 200:
        raise RuntimeError(response.status_code)
    mr_data = response.json()
    return mr_data
        

def get_mrs_to_deploy(mr_data: dict) -> list:
    mrs_to_deploy = []
    for mr in mr_data:
        changes_required = False
        labels = mr.get('labels', [])
        for label in labels:
            if (isinstance(label, str) and label in ('Status::Changes Required', 'Status::Awaiting Dependency', 'Status::Work in Progress', 'Status::Abandoned')):
                changes_required = True
                break
        if changes_required or mr['title'].lower().startswith('draft:'):
            continue
        mrs_to_deploy.append(mr['iid'])
    return sorted(mrs_to_deploy)


def git_fetch():
    print('git fetching latest...')
    subprocess.run(['git', 'fetch', '-ap']).check_returncode()


def git_reset():
    print('git reset...')
    subprocess.run(['git', 'reset', '--hard', 'origin/master']).check_returncode()


def try_merge_mr(mr: int) -> bool:
    return_code = subprocess.run(['git', 'merge', '--no-edit', f'origin/merge-requests/{mr}']).returncode

    if return_code == 0:
        return True

    subprocess.run(['git', 'merge', '--abort']).check_returncode()
    return False


def merge_mrs(mrs_to_deploy: list) -> list:
    succeeded = []
    failed = []
    for mr in mrs_to_deploy:
        print('merging', mr)
        if try_merge_mr(mr):
            succeeded.append(mr)
        else:
            failed.append(mr)
    if failed:
        print('failed to merge', *failed)
    return succeeded


def generate_forum_post(merged_mrs: list) -> str:
    output = [f'https://gitlab.com/2009scape/2009scape/-/merge_requests/{mr}' for mr in merged_mrs]
    return '\n'.join(output)


def main():
    mr_data = fetch_mr_data()
    mrs_to_deploy = get_mrs_to_deploy(mr_data)
    print('MRs to deploy:', *mrs_to_deploy)

    git_fetch()
    git_reset()
    merged_mrs = merge_mrs(mrs_to_deploy)
    print(generate_forum_post(merged_mrs))


if __name__ == '__main__':
    main()
